﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour {

  public Rigidbody2D body;

	// Use this for initialization
	void Start () {
    body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
    float dx = Input.GetAxis("Horizontal");
    float dy = Input.GetAxis("Vertical");
    float turn = Input.GetAxis("Turn");
    // Vector2 vector = new Vector2(dx, dy) * 10;
    Vector2 vector = dx * body.transform.right + dy * body.transform.up;
	  body.AddForce(vector * 80);
    body.AddTorque(turn * 30);
	}
}
