﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder {
  public List<Vector2> nodes;
  public List<Vector2> bestPath;
  public GameObject chaser;
  public GameObject target;
  public Vector2 nextNode;
  public float minimumDistance = Mathf.Infinity;
  public bool connects = false;
  public int nodesLength = 6;
  public float nodeStep = 20;
  public int pathDepth = 4;
  public LayerMask layerMask;

  public Pathfinder(GameObject chaser, GameObject target, LayerMask layerMask) {
    this.chaser = chaser;
    this.target = target;
    this.layerMask = layerMask;
    bestPath = new List<Vector2>();
    GetNodes();
    GetBestPath();
  }

  void GetNodes() {
    nodes = new List<Vector2>();
    Vector2 oldPosition = chaser.transform.position;
    for (int i = 0; i < nodesLength; i++) {
      nodes.Add(oldPosition + nodeStep * Random.insideUnitCircle);
      oldPosition = nodes[i];
    }
  }

  public void GetBestPath () {
    Vector2 start = chaser.transform.position;
    Vector2 end = target.transform.position;
    nextNode = end;
    connects = false;
    minimumDistance = Mathf.Infinity;
    List<List<Vector2>> paths = GetPaths(pathDepth, start, end);
    foreach (List<Vector2> path in paths) {
      float distance = GetDistance(path);
      if (distance < minimumDistance) {
        bestPath = path;
        nextNode = path[1];
        connects = true;
        minimumDistance = distance;
      }
    }
  }

  List<List<Vector2>> GetPaths(int depth, Vector2 start, Vector2 end) {
    List<List<Vector2>> paths = new List<List<Vector2>>();

    if (IsClear(start, end)) {
      List<Vector2> myPath = new List<Vector2>();
      myPath.Add(start);
      myPath.Add(end);
      paths.Add(myPath);
    } else if (depth > 0) {
      for (int i = 0; i < nodesLength; i++) {
        if (IsClear(start, nodes[i])) {
          List<List<Vector2>> tails = GetPaths(depth - 1, nodes[i], end);
          foreach (List<Vector2> tail in tails) {
            List<Vector2> myPath = new List<Vector2>();
            myPath.Add(start);
            myPath.AddRange(tail);
            paths.Add(myPath);
          }
        }
      }
    }

    return paths;
  }

  bool IsClear (params Vector2[] path) {
    for(int i = 1; i < path.Length; i++) {
      Vector2 direction = path[i] - path[i - 1];
      RaycastHit2D hit = Physics2D.Raycast(path[i - 1], direction, direction.magnitude, layerMask);

      if (hit.collider != null) {
        return false;
      }
    }
    return true;
  }

  float GetDistance (List<Vector2> path) {
    float distance = 0;

    for(int i = 1; i < path.Count; i++) {
      distance += (path[i] - path[i - 1]).magnitude;
    }

    return distance;
  }
}

public class Chase : MonoBehaviour {

  public GameObject target;
  public Rigidbody2D body;
  public LayerMask layerMask;
  public List<GameObject> waypoints;

  private Pathfinder currentPathfinder;
  private Pathfinder alternatePathfinder;

	// Use this for initialization
	void Start () {
		body = GetComponent<Rigidbody2D>();
    currentPathfinder = GetNewPathfinder();
    alternatePathfinder = GetNewPathfinder();
	}
	
	// Update is called once per frame
	void Update () {
    ShowPath();
    Turn();
    if (currentPathfinder.connects) {
      WalkForward();
      currentPathfinder.GetBestPath();
      alternatePathfinder = GetNewPathfinder();
      if (alternatePathfinder.minimumDistance < currentPathfinder.minimumDistance) {
        currentPathfinder = alternatePathfinder;
      }
    } else {
      currentPathfinder = GetNewPathfinder();
    }
	}

  Pathfinder GetNewPathfinder() {
    return new Pathfinder(gameObject, target, layerMask);
  }

  void ShowPath () {
    for (int i = 0; i < currentPathfinder.nodesLength; i++) {
      waypoints[i].transform.position = currentPathfinder.nodes[i];
    }

    for (int i = 1; i < currentPathfinder.bestPath.Count; i++) {
      Debug.DrawLine(currentPathfinder.bestPath[i - 1], currentPathfinder.bestPath[i], Color.red);
    }
  }

  void Turn () {
    Vector3 relativeDirection = body.GetPoint(currentPathfinder.nextNode);
    float turnDirection = Mathf.Atan2(relativeDirection.y, relativeDirection.x);
    turnDirection = Mathf.Sign(turnDirection);
    body.AddTorque(30 * turnDirection);
  }

  void WalkForward () {
		body.AddForce(20 * body.transform.right);
  }
}
